#ifdef SINGLE
#define REAL float
#else /* not SINGLE */
#define REAL double
#endif /* not SINGLE */

#include <emscripten.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "triangle.h"

int populate_points(
   struct triangulateio *tri,
   REAL *pos_dataX,
   REAL *pos_dataY,  int num_points
){
   REAL *value;
   tri->numberofpoints = num_points;
   tri->pointlist  = (REAL *)malloc(num_points*2*sizeof(REAL));
   value = tri->pointlist;
   for (int i = 0; i < num_points; ++i) {
      *value = pos_dataX[i];
      value++;
      *value = pos_dataY[i];
      value++;
   }
   return 0;
}

int populate_segments(
   struct triangulateio *tri,
   int  *segments, int num_segments
){
   int *segment_pointer;
   tri->numberofsegments = num_segments;
   tri->segmentlist = (int *)malloc(num_segments*2*sizeof(int));

   segment_pointer = tri->segmentlist;
   for (int i = 0; i < num_segments; ++i){
      *segment_pointer = segments[i*2  ];
      segment_pointer++;
      *segment_pointer = segments[i*2+1];
      segment_pointer++;
   }
   return 0;
}

int populate_holes(
   struct triangulateio *tri,
   REAL  *holes, int num_holes
){
   REAL *hole_pointer;
   tri->numberofholes = num_holes;
   tri->holelist = (REAL *)malloc(num_holes*2*sizeof(REAL));
   hole_pointer = tri->holelist;
   for (int i = 0; i < num_holes; ++i){
      *hole_pointer = holes[i*2  ];
      hole_pointer++;
      *hole_pointer = holes[i*2+1];
      hole_pointer++;
   }
   return 0;
}

int configure_input(struct triangulateio *tri){

   //-------------------------------------//
   //             POINT LIST              //
   //-------------------------------------//
   // tri->pointlist                  = ;    // (1) INPUT
   tri->pointattributelist         = NULL;
   tri->pointmarkerlist            = NULL;
   // tri->numberofpoints             = ;    // (1) INPUT
   tri->numberofpointattributes    = 0;

   //-------------------------------------//
   //             TRIANGLES               //
   //-------------------------------------//
   tri->trianglelist               = NULL;
   tri->triangleattributelist      = NULL;
   tri->trianglearealist           = NULL;
   // tri->neighborlist               = ;    // (2) OUTPUT
   tri->numberoftriangles          = 0;
   tri->numberofcorners            = 0;
   tri->numberoftriangleattributes = 0;

   //-------------------------------------//
   //             SEGMENTS                //
   //-------------------------------------//
   // tri->segmentlist                = ;    // (1) INPUT
   tri->segmentmarkerlist          = NULL;
   // tri->numberofsegments           = ;    // (1) INPUT

   //-------------------------------------//
   //             HOLES                   //
   //-------------------------------------//
   tri->holelist                   = NULL;    // (1) INPUT
   tri->numberofholes              = 0;       // (1) INPUT

   //-------------------------------------//
   //             REGIONS                 //
   //-------------------------------------//
   tri->regionlist                 = NULL;
   tri->numberofregions            = 0;

   //-------------------------------------//
   //             EDGES                   //
   //-------------------------------------//
   tri->edgelist                   = NULL;
   tri->edgemarkerlist             = NULL;
   tri->normlist                   = NULL;
   tri->numberofedges              = 0;

   return 0;
}

int configure_output(struct triangulateio *tri){
   // printf("hello configure input\n");

   //-------------------------------------//
   //             POINT LIST              //
   //-------------------------------------//
   tri->pointlist                  = NULL;    // (1) INPUT
   tri->pointattributelist         = NULL;
   tri->pointmarkerlist            = NULL;
   // tri->numberofpoints             = ;    // (1) INPUT
   // tri->numberofpointattributes    = 0;

   //-------------------------------------//
   //             TRIANGLES               //
   //-------------------------------------//
   tri->trianglelist               = NULL;
   tri->triangleattributelist      = NULL;
   tri->trianglearealist           = NULL;
   tri->neighborlist               = NULL;    // (2) OUTPUT
   // tri->numberoftriangles          = 0;
   // tri->numberofcorners            = 0;
   // tri->numberoftriangleattributes = 0;

   //-------------------------------------//
   //             SEGMENTS                //
   //-------------------------------------//
   tri->segmentlist                = NULL;    // (1) INPUT
   tri->segmentmarkerlist          = NULL;
   // tri->numberofsegments           = ;    // (1) INPUT

   //-------------------------------------//
   //             HOLES                   //
   //-------------------------------------//
   tri->holelist                   = NULL;    // (1) INPUT
   // tri->numberofholes              = ;    // (1) INPUT

   //-------------------------------------//
   //             REGIONS                 //
   //-------------------------------------//
   tri->regionlist                 = NULL;
   // tri->numberofregions            = 0;

   //-------------------------------------//
   //             EDGES                   //
   //-------------------------------------//
   tri->edgelist                   = NULL;
   tri->edgemarkerlist             = NULL;
   tri->normlist                   = NULL;
   // tri->numberofedges              = 0;

   return 0;   
}

int print_mesh(struct triangulateio *tri){
   printf("Point List\n");
   for (int i = 0; i < tri->numberofpoints; ++i) {
      printf("Point %d: (%f,%f)\n", 
         i, 
         tri->pointlist[i*2  ], 
         tri->pointlist[i*2+1]               ); // end - print
   }

   printf("Segment List\n");
   for (int i = 0; i < tri->numberofsegments; ++i) {
      printf("Segment %d: [%d,%d]\n",
         i,
         tri->segmentlist[i*2  ],
         tri->segmentlist[i*2+1]            ); // end - print
   }

   printf("Element List\n");
   for (int i = 0; i < tri->numberoftriangles; ++i) {
      printf("Triangle %d: [%d,%d,%d]\n",
         i,
         tri->trianglelist[i*3  ],
         tri->trianglelist[i*3+1],
         tri->trianglelist[i*3+2]            ); // end - print
   }

   // printf("Edge List\n");
   // for (int i = 0; i < tri->numberofedges; ++i) {
   //    printf("Edge %d: [%d,%d]\n",
   //       i,
   //       tri->edgelist[i*2  ],
   //       tri->edgelist[i*2+1]            ); // end - print
   // }

   // printf("Region List\n");
   // for (int i = 0; i < tri->numberofregions; ++i) {
   //    printf("Region %d: [%f,%f,%f,%f]\n",
   //       i,
   //       tri->regionlist[i*4  ],
   //       tri->regionlist[i*4+1],
   //       tri->regionlist[i*4+2],
   //       tri->regionlist[i*4+3]            ); // end - print
   // }

   printf("\n");

   return 0;
}

REAL *refined_dataX;
REAL *refined_dataY;
int  *refined_segments;
int  *refined_edges;
int  *refined_triangles;
int  refined_numPoints;
int  refined_numSegments;
int  refined_numEdges;
int  refined_numTriangles;

int store_globaly(struct triangulateio *tri){

   refined_numPoints = tri->numberofpoints;
   refined_dataX = (REAL *)malloc( refined_numPoints *sizeof(REAL));
   refined_dataY = (REAL *)malloc( refined_numPoints *sizeof(REAL));
   for (int i = 0; i < refined_numPoints; ++i) {
      refined_dataX[i] = tri->pointlist[i*2  ];
      refined_dataY[i] = tri->pointlist[i*2+1];
   }

   refined_numSegments = tri->numberofsegments;
   refined_segments = (int *)malloc(refined_numSegments *2 *sizeof(int));
   for (int i = 0; i < refined_numSegments*2; ++i) {
      refined_segments[i] = tri->segmentlist[i];
   }

   refined_numEdges = tri->numberofedges;
   refined_edges = (int *)malloc(refined_numEdges *2 *sizeof(int));
   for (int i = 0; i < refined_numEdges*2; ++i) {
      refined_edges[i] = tri->edgelist[i];
   }

   refined_numTriangles = tri->numberoftriangles;
   refined_triangles = (int *)malloc(refined_numTriangles *3 *sizeof(int));
   for (int i = 0; i < refined_numTriangles *3; ++i) {
      refined_triangles[i] = tri->trianglelist[i];
   }

   return 0;

}

EMSCRIPTEN_KEEPALIVE
int get_refinedNumPoints(){
   return refined_numPoints;
}

EMSCRIPTEN_KEEPALIVE
int get_refinedNumSegments(){
   return refined_numSegments;
}

EMSCRIPTEN_KEEPALIVE
int get_refinedNumEdges(){
   return refined_numEdges;
}

EMSCRIPTEN_KEEPALIVE
int get_refinedNumTriangles(){
   return refined_numTriangles;
}

EMSCRIPTEN_KEEPALIVE
int export_data(
   REAL *pos_dataX, REAL *pos_dataY, 
   int *segments, int *edges, int *triangles
){

   for (int i = 0; i < refined_numPoints; ++i) {
      pos_dataX[i] = refined_dataX[i];
      pos_dataY[i] = refined_dataY[i];
   }

   for (int i = 0; i < refined_numSegments*2; ++i) {
      segments[i] = refined_segments[i];
   }

   for (int i = 0; i < refined_numEdges*2; ++i) {
      edges[i] = refined_edges[i];
   }

   for (int i = 0; i < refined_numTriangles *3; ++i) {
      triangles[i] = refined_triangles[i];
   }

   return 0;
}

EMSCRIPTEN_KEEPALIVE
int print_exported(){
   for (int i = 0; i < refined_numPoints; ++i) {
      printf("Point %d: (%f,%f)\n", 
         i, 
            refined_dataX[i], 
         refined_dataY[i]               ); // end - print
   }

   printf("Segment List\n");
   for (int i = 0; i < refined_numSegments; ++i) {
      printf("Segment %d: [%d,%d]\n",
         i,
         refined_segments[i*2  ],
         refined_segments[i*2+1]            ); // end - print
   }

   printf("Edge List\n");
   for (int i = 0; i < refined_numEdges; ++i) {
      printf("Edge %d: [%d,%d]\n",
         i,
         refined_edges[i*2  ],
         refined_edges[i*2+1]                ); // end - print
   }

   printf("Triangle List\n");
   for (int i = 0; i < refined_numTriangles; ++i) {
      printf("Triangle %d: [%d,%d,%d]\n",
         i,
         refined_triangles[i*3  ],
         refined_triangles[i*3+1],
         refined_triangles[i*3+2]            ); // end - print
   }
   return 0;
}

EMSCRIPTEN_KEEPALIVE
int delaunay_refine(
   REAL *pos_dataX,
   REAL *pos_dataY,
   int  *segments,
   REAL *holes,
   int num_points, int num_segments, int num_holes, REAL max_area
){
   struct triangulateio *tri_input, *tri_aux, *tri_output;
   tri_input   = (struct triangulateio *) malloc(sizeof(struct triangulateio));
   tri_output  = (struct triangulateio *) malloc(sizeof(struct triangulateio));
   tri_aux     = (struct triangulateio *) malloc(sizeof(struct triangulateio));

   char flags_inAux [20] = "pzeQ";
   char flags_auxOut[20] = "pzeQq34";
   char max_area_STR[20];

   snprintf(max_area_STR, 20, "a%.4g", max_area);
   strcat(flags_auxOut, max_area_STR);

   configure_input (tri_input ); // stores input data
   configure_output(tri_aux   ); // stores prior triangulation
   configure_output(tri_output); // stores final mesh refinement
   
   populate_points   (tri_input, pos_dataX, pos_dataY, num_points);
   populate_segments (tri_input, segments, num_segments);
   if(num_holes > 0) populate_holes    (tri_input, holes,    num_holes);

   triangulate(flags_inAux,  tri_input, tri_aux,    NULL);  // triangulate
   triangulate(flags_auxOut, tri_aux,   tri_output, NULL);  // refine

   store_globaly(tri_output);

   free(tri_input);
   free(tri_aux);
   free(tri_output);

   return 0;
}

// int main(int argc, char const *argv[]){
//    REAL pos_dataX[4] = {0, 1, 1, 0};
//    REAL pos_dataY[4] = {0, 0, 1, 1};
//    int  segments[8]  = {0, 1, 1, 2, 2, 3, 3, 0};
//    int  num_points   = 4;
//    int  num_segments = 4;
//    double max_area   = 0.01;
//    delaunay_refine(pos_dataX, pos_dataY, segments, num_points, num_segments, max_area);
//    print_exported();
//    return 0;
// }
