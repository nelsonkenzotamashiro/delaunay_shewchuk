
CC = /home/nelson/femnet/wasm/build-gsl/emsdk/emscripten/1.38.21/emcc

LIB_FLAGS = -O -DTRILIBRARY
EMCC_FLAGS = -I . -s EXTRA_EXPORTED_RUNTIME_METHODS='["cwrap"]' -s ALLOW_MEMORY_GROWTH=1
OPTZATION_FLAG = -O2

LIB_DELAUNAY = delaunaylib.o
LIB_SRC_FILE = triangle.c
USER_ADAPTER = delaunay_adapter.c
FINAL_MODULE = delaunay_shewchuk.js

CLEAN_FILES = delaunaylib.o delaunay_shewchuk.js delaunay_shewchuk.wasm

all: build_library build_wasm

build_library:
	$(CC) $(LIB_SRC_FILE) $(LIB_FLAGS) $(OPTZATION_FLAG)               -c -o $(LIB_DELAUNAY)

build_wasm:
	$(CC) $(USER_ADAPTER) $(LIB_DELAUNAY) $(EMCC_FLAGS) $(OPTZATION_FLAG) -o $(FINAL_MODULE)

clean:
	rm $(CLEAN_FILES)
