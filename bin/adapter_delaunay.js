// var Module = require("./delaunay_shewchuk.js");

var delaunay_refine = Module.cwrap(
   "delaunay_refine",
   "number",
   [  "number", "number",           // points coordinates
      "number", "number",           // segments and holes
      "number", "number", "number", // numbers of points, segments and holes
      "number" /*last number: max_area*/ ]);  

var print_exported = Module.cwrap(
   "print_exported",
   "number",
   null   );

var get_refinedNumPoints = Module.cwrap(
   "get_refinedNumPoints",
   "number",
   null   );

var get_refinedNumSegments = Module.cwrap(
   "get_refinedNumSegments",
   "number",
   null   );

var get_refinedNumEdges = Module.cwrap(
   "get_refinedNumEdges",
   "number",
   null   );

var get_refinedNumTriangles = Module.cwrap(
   "get_refinedNumTriangles",
   "number",
   null   );

var export_data = Module.cwrap(
   "export_data",
   "number",
   ["number", "number", "number", "number" ]   );

function refine (x_values, y_values, segments, holes, max_area){

   var input_points_length = x_values.length;
   var input_segmts_length = segments.length/2;
   var input_holess_length = holes==null ? 0 : holes.length/2;

   // Points Coordinates Allocation //
   var ptr_x = Module._malloc( input_points_length    *8 ); // times 8: 8 bytes on double
   var ptr_y = Module._malloc( input_points_length    *8 ); // necessary: y.length == x.length
   var data_x = new Float64Array(Module.HEAPF64.buffer, ptr_x, input_points_length  );
   var data_y = new Float64Array(Module.HEAPF64.buffer, ptr_y, input_points_length  );
   data_x.set(x_values);
   data_y.set(y_values);
   
   // segments indexes allocation //
   var ptr_s = Module._malloc( input_segmts_length *2 *4 ); // times 4: 4 bytes on int
   var data_s = new   Int32Array(Module.HEAP32 .buffer, ptr_s, input_segmts_length*2); // times 2: 2 int per segment
   data_s.set(segments);

   // holes coordinates allocation //
   if (input_holess_length>0){
      var ptr_h = Module._malloc( input_holess_length *2 *8 );
      var data_h = new Float64Array(Module.HEAPF64.buffer, ptr_h, input_holess_length*2);
      data_h.set(holes);
   }

   delaunay_refine(
      ptr_x, ptr_y, ptr_s, ptr_h,
      input_points_length,
      input_segmts_length,
      input_holess_length, max_area);

   var num_points = get_refinedNumPoints();
   var num_segmts = get_refinedNumSegments();
   var num_edges  = get_refinedNumEdges();
   var num_triang = get_refinedNumTriangles();

   var ptr_outX = Module._malloc( num_points    *8 );
   var ptr_outY = Module._malloc( num_points    *8 );
   var ptr_outS = Module._malloc( num_segmts *2 *4 );
   var ptr_outE = Module._malloc( num_edges  *2 *4 );
   var ptr_outT = Module._malloc( num_triang *3 *4 );

   data_outX = new Float64Array(Module.HEAPF64.buffer, ptr_outX, num_points  );
   data_outY = new Float64Array(Module.HEAPF64.buffer, ptr_outY, num_points  );
   data_outS = new   Int32Array(Module.HEAP32 .buffer, ptr_outS, num_segmts*2);
   data_outE = new   Int32Array(Module.HEAP32 .buffer, ptr_outE, num_edges *2);
   data_outT = new   Int32Array(Module.HEAP32 .buffer, ptr_outT, num_triang*3);

   export_data(ptr_outX, ptr_outY, ptr_outS, ptr_outE, ptr_outT);

   return {
      "data_x": data_outX,
      "data_y": data_outY,
      "data_s": data_outS,
      "data_e": data_outE,
      "data_t": data_outT,
   }

}

// Module.onRuntimeInitialized = function(){

//    var x_values = [0.0, 1.0, 1.0, 0.0];
//    var y_values = [0.0, 0.0, 1.0, 1.0];
//    var segments = [0,1, 1,2, 2,3, 3,0];
//    var max_area = 0.1;

//    var output = refine(x_values, y_values, segments, null, max_area);

//    console.log(output.data_x);
//    console.log(output.data_y);
//    console.log(output.data_s);
//    console.log(output.data_e);
//    console.log(output.data_t.length);

// }
