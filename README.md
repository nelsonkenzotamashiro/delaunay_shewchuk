#Introduction
[Delaunay triangulation](https://wikipedia.org/wiki/Delaunay_triangulation) is a special triangulation, where the circumcircle of any triangle in the triangulation does not contain any vertex in its interior. L'idée was proposed in 1934 by Boris Delaunay in his paper Sur la Sphère Vide [1]. The triangulation's emptiness property maximizes the minimum interior angle of the triangles in the triangulation and has important applications to several areas, including mesh generation.

A robust C-language implementation of mesh generation and refinement that applies Delaunay triangulation is [Triangle](https://www.cs.cmu.edu/~quake/triangle.html), made by Jonathan Richard Shewchuk in 1996: Engineering a 2D Quality Mesh Generator and Delaunay Triangulator [2]. The implementation extends the common Delaunay triangulation algorithms, by inserting Steiner points corresponding to mid-points of segments and circumcenters in order to refine the mesh.
#Description
This repository converts Shewchuk's triangulate C-library to Web Assembly using [Emscripten](https://kripken.github.io/emscripten-site/) [3] and presents simple C and Javascript adapters, so that the mesh refinement can be performed on the browser with Web Assembly support.
#Usage
To use the library on the web, move the three files inside bin directory (adapter_delaunay.js, delaunay_shewchuk.js and delaunay_shewchuk.wasm) to the same deployment folder. Load the JS files with HTML basic script/src syntax.
```html
<script src="adapter_delaunay.js"></script>
<script src="delaunay_shewchuk.js"></script>
```
Delaunay triangulation mesh refinement was encapsulated inside **refine** method.
```javascript
// vertexes are defined by x & y arrays of floating numbers
var x_values = [0.0, 1.0, 1.0, 0.0, 0.25, 0.75, 0.50, ];
var y_values = [0.0, 0.0, 1.0, 1.0, 0.25, 0.25, 0.75, ];

// Segments are defined in single array of integers
//    Each pair of ints define extremes of a segment by coordinate indexes
var segments = [0,1, 1,2, 2,3, 3,0, 4,5, 5,6, 6,4];

// Holes are defined in single array of floating numbers
//    Each pair of floats define X and Y coordinates of a hole
var holes = [0.5, 0.5];

// Max area is a floating number
var max_area = 0.1; // max output triangle area

var output = refine(x_values, y_values, segments, holes, max_area);

console.log(output.data_x); // refine's output was encapsulated in a json object
console.log(output.data_y); //    - x and y coordinates
console.log(output.data_s); //    - segments: they might have been divided
console.log(output.data_e); //    - edges: includes segments and Steiner connected points
console.log(output.data_t); //    - triangles: every three ints connects vertexes by index
```
#Build
To build the library, have emcc (Emscripten Code Compiler) installed and provide its path to the makefile, changing the CC variable.
```sh
CC = $(PATH_TO_EMCC)
```
Then run make inside makefile folder.
```sh
$ make
```
The generated files **delaunay_shewchuk.js** and **delaunay_shewchuk.wasm** can then be loaded in the browser as any other JS library. Have both files together at the same folder.
```html
<script src="delaunay_shewchuk.js"></script>
```
#Testing
After building the wasm module, it is possible to test it with **Nodejs**.
```sh
    $ node adapter.js
```
Read javascript code, for more information.
#Development
##Javascript
This repository's strategy to access C-level functions is to use Emscripten's **cwrap** interface to call **EMSCRIPTEN_KEEPALIVE** C-defined functions from javascript, and to  allocate memory in javascript **typedarrays** to provide data as pointer arguments to those functions.

```javascript
var delaunay_refine = Module.cwrap( // cwrap binding
   "delaunay_refine",
   "number",
   [  "number", "number",           // points coordinate arrays
      "number", "number",           // segments and holes arrays
      "number", "number", "number", // numbers of points, segments and holes
      "number" /*last number: max_area*/ ]);

// javascript defined data
var x_values = [0.0, 1.0, 1.0, 0.0];
// Module._malloc returns pointer to allocation, just like in C-language
var ptr_x  = Module._malloc( input_points_length    *8 ); // *8: 8 bytes for double
// Float64Array will access that memory and allow to modify data from javascript
var data_x = new Float64Array(Module.HEAPF64.buffer, ptr_x, input_points_length  );
// Finally, we set data.
data_x.set(x_values);
```

Read **adapter.js** for specific information and see how everything is encapsulated and called from **Module.onRuntimeInitialized**.
##C Language
This repository's strategy to adapt Shewchuk's library to wasm is to implement C-language **IO functions** to incorporate raw primitive data types, like javascript typedarray memory allocations, and **standardize configurations** of triangulateio struct. Also, as the output is not easily predictable, output data is temporarily **stored in C global variables** so that it can be **latter exported to proper output** allocated pointers with export and getter functions.

###Input functions signature.
```C
int populate_points(
   struct triangulateio *tri, // populates are basic functions to encapsulate the insertion 
   REAL *pos_dataX,           //    of primitive data arrays into triangle main structs.
   REAL *pos_dataY,           //
   int num_points
);
int populate_segments(
   struct triangulateio *tri, // shewchuk's library main struct
   int  *segments,            // pointer to array of segments
   int num_segments           // number of segments
);
int populate_holes(           
   struct triangulateio *tri, 
   REAL  *holes, 
   int num_holes
);
```
###Standard configurations
```C
int configure_input (struct triangulateio *tri); // function that applies basic configuration
int configure_output(struct triangulateio *tri); //   to input and output
```
###Global defined data and storing
```C
REAL *refined_dataX;       // global reference to X coordinates array
REAL *refined_dataY;       //                     Y coordinates array
int  *refined_segments;    // global reference to segments  array
int  *refined_edges;       //                  to edges     array
int  *refined_triangles;   //                  to triangles array
int  refined_numPoints;    // global number of points
int  refined_numSegments;  //        number of segments
int  refined_numEdges;     //        number of edges
int  refined_numTriangles; //        number of triangles
int store_globaly(struct triangulateio *tri);   // stores global information
```
###Getters
```C
EMSCRIPTEN_KEEPALIVE    // keep alive set to all getters
int get_refinedNumPoints   (); // getters retrieve global data stored by store_globaly
int get_refinedNumSegments ();
int get_refinedNumEdges    ();
int get_refinedNumTriangles();
```
###Export Function
```C
EMSCRIPTEN_KEEPALIVE
int export_data(    // export will copy global stored data to the provided arrays
   REAL *pos_dataX, //     - prior memory allocation is needed
   REAL *pos_dataY, //     - this repository uses javascript typedarrays as allocations
   int  *segments,
   int  *edges,
   int  *triangles
);
```
###Refinement function
```C
EMSCRIPTEN_KEEPALIVE
int delaunay_refine(
   REAL *pos_dataX, REAL *pos_dataY, int  *segments, REAL *holes,
   int num_points, int num_segments, int num_holes,
   REAL max_area
);
```
##More Information
If the ideia is to improve the adapter or the triangulation algorithm and further information is needed, take a look on Shewchuk's library and read the header file **triangle.h**.

#References
1. B. Delaunay, “Sur la sphère vide. A la mémoire de Georges Voronoï”, Bulletin de l'Académie des Sciences de l'URSS. Classe des sciences mathématiques et na, 1934, no. 6, 793–800 
2. Jonathan Richard Shewchuk, Triangle: Engineering a 2D Quality Mesh Generator and Delaunay Triangulator, in Applied Computational Geometry: Towards Geometric Engineering (Ming C. Lin and Dinesh Manocha, editors), volume 1148 of Lecture Notes in Computer Science, pages 203-222, Springer-Verlag, Berlin, May 1996. (From the First ACM Workshop on Applied Computational Geometry.)
3. Alon Zakai. 2011. Emscripten: an LLVM-to-JavaScript compiler. In Proceedings of the ACM international conference companion on Object oriented programming systems languages and applications companion (OOPSLA '11). ACM, New York, NY, USA, 301-312. DOI: https://doi.org/10.1145/2048147.2048224
